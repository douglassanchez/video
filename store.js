import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import reducer from './reducers/index';
import storage from 'redux-persist/lib/storage';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';

/* const initialState = {
  suggestionList: [],
  categoryList: [],
}

const store = createStore(
  reducer,
  initialState
) */

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['navigation'] // Para no persistir este dato
}

const navigationMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.navigation
)

const persistedReducer = persistReducer(persistConfig, reducer)
const store = createStore(persistedReducer, applyMiddleware(navigationMiddleware))
const persistor = persistStore(store)

/* export default () => {
  const store = createStore(persistedReducer)
  const persistor = persistStore(store)
  return { store, persistor }
} */

export { store, persistor };