import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import Layout from '../../videos/components/suggestion-list-layout';
import Empty from '../../videos/components/empty';
import Separator from '../../videos/components/vertical-separator';
import Suggestion from '../../videos/components/suggestion';

function mapStateToProps(state) {
  return {
    list: state.videos.categoryList
  }
}

class Category extends Component {
  renderEmpty = () => <Empty text="No hay sugerencias :(" />

  ItemSeparator = () => <Separator color="gray"/>

  // Cuando no tenemos en los elementos de mi array una propiedad "key".
  keyExtractor = (item) => item.id.toString();

  viewMovie = (item) => {
    this.props.dispatch({
      type: 'SET_SELECTED_MOVIE',
      payload: {
        movie: item,
      }
    })

    this.props.dispatch(
      NavigationActions.navigate({
        routeName: 'Movie'
      })
    )
  }

  renderItem = ({item}) => {
    return (
      <Suggestion 
        {...item}
        onPress={() => {this.viewMovie(item)}} 
      />
    );
  }

  render() {
    return (
      <Layout title={`${this.props.navigation.getParam('genre', 'Category')}`}>
        <FlatList
          keyExtractor={this.keyExtractor}
          data={this.props.list}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.ItemSeparator}
          renderItem={this.renderItem}
        />
      </Layout>
    );
  }
}

export default connect(mapStateToProps)(Category);
