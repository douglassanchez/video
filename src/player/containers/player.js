import React, { Component } from 'react';
import Video from 'react-native-video';
import { StyleSheet, ActivityIndicator } from 'react-native';
import Layout from '../components/player-layout';
import ControlLayout from '../components/control-layout';
import PlayPause from '../components/play-pause';

class Player extends Component {
  state = {
    loading: true,
    paused: false,
  }

  onBuffer = ({ isBuffering }) => {
    this.setState({
      loading: isBuffering
    });
  }

  onLoad = () => {
    this.setState({
      loading: false
    });
  }

  playPause = () => {
    this.setState({
      paused: !this.state.paused
    });
  }

  render() {
    return (
      <Layout 
        loading={this.state.loading}
        video={
          <Video 
            style={styles.container}
            resizeMode='contain'
            source={{uri: 'https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4'}}
            onBuffer={this.onBuffer}
            onLoad={this.onLoad}
            paused={this.state.paused}
          />
        }
        loader={
          <ActivityIndicator color="#fff"/>
        }
        controls={
          <ControlLayout>
            <PlayPause 
              onPress={this.playPause}
              paused={this.state.paused}
            />
          </ControlLayout>
        }
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0
  }
});

export default Player;