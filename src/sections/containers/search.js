import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { 
  TextInput,
  StyleSheet 
} from 'react-native';

import Api from '../../../utils/api';

class Search extends Component {
  state = {
    text: ''
  }

  handleSubmit = async () => {
    console.log(this.state.text)
    const movies = await Api.searchMovie(this.state.text)

    this.props.dispatch({
      type: 'SET_SELECTED_MOVIE',
      payload: {
        movie: movies[0]
      }
    })
    
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: 'Movie'
      })
    )

    console.log(movies)
  }

  handleChangeText = (text) => {
    this.setState({
      text
    })
  }

  render() {
    return (
      <TextInput
        style={styles.input}
        placeholder="Busca tu película favorita"
        autoCorrect={false}
        autoCapitalize="none"
        underlineColorAndroid="transparent"
        onSubmitEditing={this.handleSubmit}
        onChangeText={this.handleChangeText}
      />
    )
  }
}

const styles = StyleSheet.create({
  input: {
    padding: 16,
    fontSize: 16,
    borderWidth: 1,
    borderColor: '#eaeaea',
  }
})

export default connect(null)(Search);