import React from 'react';
import {
  Image, 
  SafeAreaView, 
  ScrollView,
  StyleSheet 
} from 'react-native';
import { DrawerItems } from 'react-navigation';

const Drawer = (props) => (
  <ScrollView>
    <SafeAreaView>
      <Image 
        source={require('../../../assets/logo.png')}
        style={styles.logo}
      >
      </Image>
      <DrawerItems {...props}/>
    </SafeAreaView>
  </ScrollView>
);

const styles = StyleSheet.create({
  logo: {
    width: 80,
    height: 26,
    resizeMode: 'contain',
    marginVertical: 20,
    marginLeft: 10,
  }
})

export default Drawer;