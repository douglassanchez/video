import React from 'react';
import { 
  View,
  Image,
  StyleSheet,
  ActivityIndicator
} from 'react-native';

const Loading = (props) => (
  <View style={styles.container}>
    <Image 
      source={require('../../../assets/logo.png')}
      style={styles.logo}
    />
    <ActivityIndicator />
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 200, 
    height: 80,
    resizeMode: 'contain',
    marginBottom: 16,
  }
})

export default Loading;