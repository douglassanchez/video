import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { NavigationActions } from 'react-navigation';

import Layout from '../components/category-list-layout';
import Empty from '../components/empty';
import Separator from '../../sections/components/horizontal-separator';
import Category from '../components/category';
import { connect } from 'react-redux';

class CategoryList extends Component {
  renderEmpty = () => <Empty text="No hay sugerencias :(" />

  ItemSeparator = () => <Separator color="gray"/>

  // Cuando no tenemos en los elementos de mi array una propiedad "key".
  keyExtractor = (item) => item.id.toString();

  viewCategory = (item) => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: 'Category',
        params: {
          genre: (item.genres) ? item.genres[0] : 'No Title'
        }
      })
    )
  }

  renderItem = ({item}) => {
    return (
      <Category 
        {...item}
        onPress={ () => { this.viewCategory(item)} }
      />
    );
  }

  render() {
    return (
      <Layout>
        <FlatList
          horizontal
          keyExtractor={this.keyExtractor}
          data={this.props.list}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.ItemSeparator}
          renderItem={this.renderItem}
        />
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  console.log(state.videos.categoryList)
  return {
    list: state.videos.categoryList
  }
}

export default connect(mapStateToProps)(CategoryList);