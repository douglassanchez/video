import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

import Layout from '../components/suggestion-list-layout';
import Empty from '../components/empty';
import Separator from '../components/vertical-separator';
import Suggestion from '../components/suggestion';

function mapStateToProps(state) {
  return {
    list: state.videos.suggestionList
  }
}

class SuggestionList extends Component {
  renderEmpty = () => <Empty text="No hay sugerencias :(" />

  ItemSeparator = () => <Separator color="gray"/>

  // Cuando no tenemos en los elementos de mi array una propiedad "key".
  keyExtractor = (item) => item.id.toString();

  viewMovie = (item) => {
    this.props.dispatch({
      type: 'SET_SELECTED_MOVIE',
      payload: {
        movie: item,
      }
    })

    this.props.dispatch(
      NavigationActions.navigate({
        routeName: 'Movie'
      })
    )
  }

  renderItem = ({item}) => {
    return (
      <Suggestion 
        {...item}
        onPress={() => {this.viewMovie(item)}} 
      />
    );
  }

  render() {
    return (
      <Layout title="Recomendado para ti">
        <FlatList
          keyExtractor={this.keyExtractor}
          data={this.props.list}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.ItemSeparator}
          renderItem={this.renderItem}
        />
      </Layout>
    );
  }
}

export default connect(mapStateToProps)(SuggestionList);
