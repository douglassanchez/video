import React from 'react';
import { 
  Text, 
  ImageBackground, 
  StyleSheet,
  TouchableOpacity 
} from 'react-native';

const Category = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <ImageBackground 
      style={styles.container}
      source={{uri: props.background_image}}
    >
      <Text style={styles.genre}>
        { (props.genres) ? props.genres[0] : 'No Title' }
      </Text>
    </ImageBackground>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    width: 250,
    height: 100,
    borderRadius: 10,
    overflow: 'hidden',
    alignItems: 'center', 
    justifyContent: 'center',
  },
  genre: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, .75)',
    textShadowOffset: {
      width: 2,
      height: 2
    },
    textShadowRadius: 0,
  }
});

export default Category;