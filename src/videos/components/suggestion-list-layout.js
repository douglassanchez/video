import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const SuggestionListLayout = (props) => (
  <View style={styles.container}>
    <Text style={styles.title}>{props.title}</Text>
    {props.children}
  </View>
)

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    flex: 1,
  },
  title: {
    color: '#4c4c4c',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 16,
    marginLeft: 16,
  }
});

export default SuggestionListLayout;