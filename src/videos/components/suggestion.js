import React from 'react';
import { 
  View, 
  Image, 
  Text, 
  StyleSheet,
  TouchableOpacity
} from 'react-native';

const Suggestion = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <View style={styles.container}>
      <View style={styles.left}>
        <Image 
          style={styles.cover} 
          source={{uri: props.medium_cover_image}}
        />
        <View style={styles.gender}>
          <Text style={styles.genderText}>{props.genres[0]}</Text>
        </View>
      </View>

      <View style={styles.right}>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.year}>{props.year}</Text>
        <Text style={styles.rating}>{props.rating}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  cover: {
    height: 150,
    width: 100,
    resizeMode: 'contain',
  },
  right: {
    paddingLeft: 16,
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 18, 
    color: '#44546b',
  },
  year: {
    backgroundColor: '#70b124',
    borderRadius: 5,
    paddingVertical: 4,
    paddingHorizontal: 6,
    color: '#fff',
    fontSize: 11,
    overflow: 'hidden',
    alignSelf: 'flex-start',
  },
  rating: {
    color: '#6b6b6b',
    fontSize: 14,
    fontWeight: 'bold',
  },
  gender: {
    backgroundColor: '#000',
    position: 'absolute',
    left: 0,
    top: 0,
  },
  genderText: {
    color: '#fff',
    fontSize: 11,
    paddingVertical: 5, 
    paddingHorizontal: 7,
  }
});

export default Suggestion;