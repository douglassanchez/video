import React from 'react';
import { 
  createStackNavigator, 
  createBottomTabNavigator,
  createSwitchNavigator,
  createDrawerNavigator
} from 'react-navigation';

import Home from './screens/containers/home';
import Movie from './screens/containers/movie';
import Category from './screens/containers/category';
import Header from './sections/components/header';
import About from './screens/containers/about';
import Profile from './screens/containers/profile';
import Lucky from './screens/containers/lucky';
import Icon from './sections/components/icon';
import Login from './screens/containers/login';
import Loading from './screens/containers/loading';
import DrawerComponent from './sections/components/drawer';

const Main = createStackNavigator(
  {
    Home: Home,
    Category,
  },
  // Configuraciones generales
  {
    navigationOptions: {
      header: Header,
    },
    cardStyle: {
      backgroundColor: '#fff'
    }
  }
);

const TabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: Main,
      navigationOptions: {
        title: 'Inicio',
        tabBarIcon: <Icon icon="🏠"/>
      }
    },
    About: {
      screen: About,
      navigationOptions: {
        title: 'Nosotros',
        tabBarIcon: <Icon icon="😎" />
      }
    },
    Lucky: {
      screen: Lucky,
      navigationOptions: {
        title: 'Suerte',
        tabBarIcon: <Icon icon="☘️" />
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: 'Perfil',
        tabBarIcon: <Icon icon="👨🏻‍💻" />
      }
    },
  },

  // Configuración General
  {
    tabBarOptions: {
      activeTintColor: '#fff',
      activeBackgroundColor: '#65a721'
    }
  }
)

const WithModal = createStackNavigator(
  {
    Main: {
      screen: TabNavigator
    },
    Movie: Movie,
  },
  {
    mode: 'modal',
    headerMode: 'none',
    cardStyle: {
      backgroundColor: '#fff'
    },
    navigationOptions: {
      gesturesEnabled: true
    }
  }
)

const DrawerNavigator = createDrawerNavigator(
  {
    Main: {
      screen: WithModal,
      navigationOptions: {
        title: 'Inicio',
        drawerIcon: <Icon icon="🏠" />
      }
    },
    Sobre: {
      screen: About,
      navigationOptions: {
        title: 'Sobre Nosotros',
        drawerIcon: <Icon icon="😎" />
      }
    },
    Suerte: {
      screen: Lucky,
      navigationOptions: {
        drawerIcon: <Icon icon="☘️" />
      }
    }
  },
  {
    drawerWidth: 200,
    drawerBackgroundColor: '#f6f6f6',
    contentComponent: DrawerComponent,
    contentOptions: {
      activeBackgroundColor: '#7aba2f',
      activeTintColor: '#fff',
      inactiveBackgroundColor: '#fff',
      inactiveTintColor: '#828282',
      itemStyle: {
        borderBottomWidth: .5,
        borderBottomColor: 'rgba(0,0,0,.5)'
      },
      labelStyle: {
        marginHorizontal: 0,
      },
      iconContainerStyle: {
        marginHorizontal: 5,
      }
    }
  }
)

const SwitchNavigator = createSwitchNavigator(
  {
    Loading: Loading,
    App: DrawerNavigator,
    Login: Login
  },

  // Configuración General 
  {
    initialRouteName: 'Loading'
  }
)

export default SwitchNavigator;